// First Task
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$onSale", totalOnSale: { $sum: 1 } } },
  { $project: { _id: 0 } },
]);
// Alternate to first task
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$onSale", totalOnSale: { $count: {} } } },
  { $project: { _id: 0 } },
]);
// Another Alternate to first task
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $count: "onSale" },
  { $project: { _id: 0 } },
]);

// Second Task
db.fruits.aggregate([
  { $match: { stock: { $gte: 20 } } },
  { $group: { _id: "$onSale", enoughStock: { $sum: 1 } } },
  { $project: { _id: 0 } },
]);

// Third Task
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } },
]);

// Fourth Task
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } },
]);

// Fifth Task
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", minPrice: { $min: "$price" } } },
]);
